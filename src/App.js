import React from 'react';

import MyMap from './components/Map/MyMap';

import 'antd/dist/antd.css';
import './App.css';

function App() {
  return <MyMap />;
}

export default App;
