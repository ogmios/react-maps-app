import Dexie from 'dexie';

// Declare db instance
var db = new Dexie('MyDatabase');

// Define Database Schema
db.version(1).stores({
  marks: '++id, name, lat, lng',
});

export default db;
