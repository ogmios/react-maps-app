import axios from 'axios';

export default axios.create({
  baseURL: `https://geolocation-db.com/`,
});
