import React, { Fragment, useState, useEffect } from 'react';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import { Row, Col, Alert, message, Spin } from 'antd';

//indexed db
import db from '../../db';
//axios url config
import API from '../../api';

//modal
import Modal from '../../core/Modal';
//form add mark
import FormAddMark from './FormAddMark';

const MyMap = () => {
  const [map, setMap] = useState({
    lat: -33.618161,
    lng: -70.681924,
    zoom: 13,
    reload: false,
    country_name: 'Chile',
    city: 'Santiago',
  });

  const [markers, setMarkers] = useState([]);
  //loading initialize in false
  const [loading, setLoading] = useState(false);
  //modal state
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [modalTitle, setModalTitle] = useState('');
  const [modalContent, setModalContent] = useState(null);

  //destructuring map state
  const { lat, lng, zoom, reload, country_name, city } = map;

  //load mark and your location data
  useEffect(() => {
    getMapMarks();
    getCurrentLocation();
  }, [reload]);

  //function get current location
  const getCurrentLocation = async () => {
    setLoading(true);
    API.get(`json/`)
      .then((res) => {
        const {
          data: { latitude, longitude, country_name, city },
        } = res;
        setMap({
          ...map,
          lat: latitude,
          lng: longitude,
          country_name,
          city,
        });
        setLoading(false);
      })
      .catch((err) => {
        message.error(
          `Ha ocurrido el siguiente error al intentar conseguir su geo-localización: ${err.message}`
        );
        setLoading(false);
      });
  };

  //get all marks
  const getMapMarks = () => {
    db.table('marks')
      .toArray()
      .then((todos) => {
        /* setMarkers([...markers, todos]); */
        setMarkers(todos);
      });
  };

  //array lat and lng
  const position = [lat, lng];

  //change state boolen to reload data after update
  const reloadMarks = () => {
    setMap({
      ...map,
      ['reload']: !reload,
    });
  };

  //function hancle click in the map, set modal states
  const handleClick = (e) => {
    const { lat, lng } = e.latlng;
    setIsVisibleModal(true);
    setModalTitle('Agrega tu punto');
    setModalContent(
      <FormAddMark
        lat={lat}
        lng={lng}
        reloadMarks={reloadMarks}
        setIsVisibleModal={setIsVisibleModal}
      />
    );
  };

  return (
    <Fragment>
      {loading ? (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '400px',
          }}
        >
          <Spin tip='Loading...' />
        </div>
      ) : (
        <Row>
          <Col span={24} style={{ marginTop: '50px' }}>
            <div
              style={{
                width: '300px',
                textAlign: 'center',
                margin: 'auto',
              }}
            >
              <h2>Crea tus puntos por el mapa</h2>
              <Alert
                message='Haz click en un punto del mapa y asignale un nombre'
                type='info'
                showIcon
              />
            </div>

            <Map
              center={position}
              zoom={zoom}
              onClick={handleClick}
              style={{ marginTop: '100px' }}
            >
              <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
              />
              <Marker position={position}>
                <Popup>{`Pais: ${country_name} - Ciudad: ${city}`}</Popup>
              </Marker>
              {markers.map((mark) => (
                <Marker position={[mark.lat, mark.lng]} key={mark.id}>
                  <Popup>{mark.name}</Popup>
                </Marker>
              ))}
            </Map>
            <Modal
              title={modalTitle}
              isVisible={isVisibleModal}
              setIsVisible={setIsVisibleModal}
            >
              {modalContent}
            </Modal>
          </Col>
        </Row>
      )}
    </Fragment>
  );
};

export default MyMap;
