import React, { useState } from 'react';
import { Form, Input, Button, message } from 'antd';
import { PushpinOutlined } from '@ant-design/icons';

import db from '../../db';

const FormAddMark = ({ lat, lng, reloadMarks, setIsVisibleModal }) => {
  const [form] = Form.useForm();
  const [inputs, setInputs] = useState({
    name: '',
  });

  const onChange = (e) => {
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value,
    });
  };

  const onFinish = () => {
    const mark = {
      name,
      lat,
      lng,
    };
    db.table('marks')
      .add(mark)
      .then(() => {
        reloadMarks();
        setIsVisibleModal(false);
        form.resetFields();
        message.success('Su punto ha sido agregado correctamente al mapa.');
      })
      .catch(() => {
        setIsVisibleModal(false);
        message.error('Ha ocurrido un error al agregar su punto.');
      });
  };

  const { name } = inputs;

  return (
    <Form
      form={form}
      name='horizontal_login'
      layout='inline'
      onFinish={onFinish}
    >
      <Form.Item name='pointName'>
        <Input
          prefix={<PushpinOutlined className='site-form-item-icon' />}
          name='name'
          placeholder='Mi casa'
          value={name}
          onChange={onChange}
        />
      </Form.Item>
      <Form.Item>
        <Button type='primary' htmlType='submit'>
          Agregar
        </Button>
      </Form.Item>
    </Form>
  );
};

export default FormAddMark;
