## React Map
#### Descripción:

Proyecto de aprendizaje en el cual se desarrolla una pantalla con un mapa que permite guardar puntos en el navegador.

## Proyecto

[Visitar proyecto](https://url2.cl/xGpWf "Tu mapa")

## Imágenes del proyecto

![Imagen 1](https://i.ibb.co/6P9cfH8/Screenshot-1.png")


## Instalación

Primero hay que clonar este repositorio. Es necesario tener instalado `node` and `npm` or `yarn` de manera global en su maquina.

Instalación:

`npm install` o `yarn`  

Ejecutar App:

`npm start` o `yarn start` 

Visitar App:

`localhost:3000`  

#### Conclusión:  

Este proyecto fue realizado en 1 día, con propósito de aprendizaje y como guía para futuros proyectos. 

Este proyecto fue realizado con `create-react-app`, leafletjs, indexeddb y el diseño con antdesign.